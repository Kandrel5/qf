﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace QF
{
    public class QF : AOPluginEntry
    {


        ConcurrentQueue<SmokeLounge.AOtomation.Messaging.Messages.N3Messages.QuestAlternativeMessage>
            N3Msgs =
        new ConcurrentQueue<SmokeLounge.AOtomation.Messaging.Messages.N3Messages.QuestAlternativeMessage>();


        MissionTerminal mt;

        Commands acmd;

        System.Data.SQLite.SQLiteConnection cnn;
        System.Data.SQLite.SQLiteCommand cmd;

        public override void Run(string pluginDir)
        {
            Chat.WriteLine("QF loaded. Type /qf help for available commands");


            if (!pluginDir.EndsWith(@"\"))
                pluginDir += @"\";


            cnn = new System.Data.SQLite.SQLiteConnection("Data Source=" + pluginDir + "aoitems.db;Version=3;");
            cnn.Open();
            cnn.DefaultTimeout = 0;
            cmd = cnn.CreateCommand();

            acmd = new Commands(DynelManager.LocalPlayer.Name, pluginDir);

            Chat.RegisterCommand("qf", acmd.OnCommand);


            Network.N3MessageReceived += N3MessageReceived;
            Game.OnUpdate += OnUpdate;
        }

        private void OnUpdate(object sender, float e)
        {

            if (acmd.Running)
            {

                if (N3Msgs.Count != 0)
                {
                    ///////////////////////////////////////////////////////////process message
                    if (N3Msgs.TryDequeue(out SmokeLounge.AOtomation.Messaging.Messages.N3Messages.QuestAlternativeMessage qm))
                    {


                        acmd.LastMissionssLevels = "";
                        for (int slot = 0; slot < qm.MissionDetails.Length; slot++)
                        {

                            MissionInfo mi = qm.MissionDetails[slot];

                            for (int i = acmd.ItemList.Count - 1; i >= 0; i--)
                            {
                                string s = acmd.ItemList[i];
                                if (mi.Description.ToUpper().Contains(s))
                                {
                                    acmd.Running = false;

                                    Chat.WriteLine("Found " + s + " in mission:" + (slot + 1).ToString());
                                    acmd.ItemList.Remove(s);

                                    return;
                                }
                            }


                            foreach (MissionItemData mid in mi.MissionItemData)
                            {

                                List<string> dbnames = lowHighNames(mid.LowId.ToString(), mid.HighId.ToString());

                                acmd.LastMissionssLevels += "[" + mid.Ql.ToString() + "]";

                                for (int i = acmd.ItemList.Count - 1; i >= 0; i--)
                                {
                                    string s = acmd.ItemList[i];

                                    if (ConcatNames(dbnames).ToUpper().Contains(s))
                                    {
                                        Chat.WriteLine("Found " + s + " in mission:" + (slot + 1).ToString());
                                        acmd.ItemList.Remove(s);
                                        acmd.Running = false;
                                        return;

                                    }
                                }


                            }
                        }

                    }
                    

                }
                else
                {
                    //can pull
                    if (mt == null)
                    {
                        if (Targeting.Target != null)
                        {
                            if (
                                Targeting.Target.Identity.Type == IdentityType.MissionTerminal
                                )
                            {

                                Item.Use(Targeting.Target.Identity);
                                mt = new MissionTerminal(Targeting.Target);

                            }
                        }
                    }
                    else
                    {
                        Item.Use(mt.Identity);
                    }

                    if (mt != null)

                        mt.RequestMissions
                    (
                        acmd.Difficulty,
                        acmd.GoodBad,
                        acmd.OrderChaos,
                        acmd.OpenHidden,
                        acmd.PhysicalMystical,
                        acmd.HeadonStealth,
                        acmd.CreditsXP
                        );
                }

            }



        }

        private void N3MessageReceived(object sender, N3Message e)
        {
            if (e.N3MessageType == N3MessageType.QuestAlternative)
            {
                N3Msgs.Enqueue((SmokeLounge.AOtomation.Messaging.Messages.N3Messages.QuestAlternativeMessage)e);
            }
        }

        private List<string> lowHighNames(string LowId, string HighId)
        {

            List<string> ret = new List<string>();

            cmd.CommandText = "select name from tblao where aoid =" + LowId;
            using (System.Data.SQLite.SQLiteDataReader dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    ret.Add(dr["Name"].ToString());
                }
            }

            cmd.CommandText = "select name from tblao where aoid =" + HighId;
            using (System.Data.SQLite.SQLiteDataReader dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    ret.Add(dr["Name"].ToString());
                }
            }


            return ret;
        }

        private string ConcatNames(List<string> names)
        {
            return names[0] + " " + names[1];
        }
    }
}
