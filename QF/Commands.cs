﻿using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.IO;

namespace QF
{

    public class Commands
    {

        string playerName = "";
        string pluginDir;

        public bool Running = false;

        public List<string> ItemList = new List<string>();

        public Commands(string plname, string pluginDir)
        {
            playerName = plname;
            this.pluginDir = pluginDir;

            string configPath = pluginDir + plname + "_qf.txt";
            if (File.Exists(configPath))
            {
                string[] tmp = File.ReadAllLines(configPath);

                foreach (string s in tmp)
                {
                    if (s.ToUpper().Trim() != "" && !s.ToUpper().Trim().StartsWith("//")) ItemList.Add(s.ToUpper().Trim());

                }
            }

            Difficulty = 6;   //1-11???
            GoodBad = 255;
            OrderChaos = 0;
            OpenHidden = 0;
            PhysicalMystical = 0;
            HeadonStealth = 0;
            CreditsXP = 0;


        }

        public void OnCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                HandleCommand(command, param, chatWindow);
            }
            catch (Exception e)
            {
                Chat.WriteLine(e);
            }
        }

        //Window testWindow;

        //ChatWindow cWindow;

        public byte Difficulty { get; internal set; }
        public byte GoodBad { get; internal set; }
        public byte OrderChaos { get; internal set; }
        public byte OpenHidden { get; internal set; }
        public byte PhysicalMystical { get; internal set; }
        public byte HeadonStealth { get; internal set; }
        public byte CreditsXP { get; internal set; }
        public string LastMissionssLevels { get; internal set; }

        private void HandleCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (param.Length == 0)
            {
                return;
            }

            switch (param[0].ToUpper())
            {
                case "RELOAD":
                    string configPath = pluginDir + playerName + "_qf.txt";
                    if (File.Exists(configPath))
                    {
                        ItemList.Clear();

                        string[] tmp = File.ReadAllLines(configPath);

                        foreach (string s in tmp)
                        {
                            if (s.ToUpper().Trim() != "" && !s.ToUpper().Trim().StartsWith("//")) ItemList.Add(s.ToUpper().Trim());

                        }
                    }
                    break;

                case "SETDIF":
                    try
                    {
                        byte b = Byte.Parse(param[1]);
                        if (b >= 0 && b <= 11)
                            Difficulty = b;
                        else
                            throw new Exception("Invalid parameter range");
                    }
                    catch
                    {
                        chatWindow.WriteLine("USAGE: /qf setdif [0-11]");
                    }
                    break;


                case "START":
                    if (ItemList.Count == 0)
                    {
                        chatWindow.WriteLine("No items to search.");
                        Running = false;
                        return;
                    }

                    Running = true;
                    chatWindow.WriteLine("QF STARTED");
                    break;

                case "STOP":
                    Running = false;
                    chatWindow.WriteLine("QF STOPPED");
                    break;

                case "STATUS":

                    chatWindow.WriteLine("Running=" + Running.ToString());
                    chatWindow.WriteLine("Last Missions levels=" + LastMissionssLevels);

                    break;


                case "LIST":
                    chatWindow.WriteLine("=========== ItemList LIST =============");
                    foreach (string s in ItemList)
                    {
                        chatWindow.WriteLine(s);
                    }
                    chatWindow.WriteLine("=======================================");
                    break;

                case "HELP":
                    chatWindow.WriteLine("QF HELP:");
                    chatWindow.WriteLine("/qf setdif [1-11]  Set mission quality level");
                    chatWindow.WriteLine("/qf start          Starts pulling           ");
                    chatWindow.WriteLine("/qf stop           Stops pulling            ");
                    chatWindow.WriteLine("/qf status         Report                   ");
                    chatWindow.WriteLine("/qf list           Show item list           ");
                    chatWindow.WriteLine("/qf reload         Loads item list from file");
                    chatWindow.WriteLine("/qf help           I dont know how you reach here!");
                    break;

            }
        }
    }



}
